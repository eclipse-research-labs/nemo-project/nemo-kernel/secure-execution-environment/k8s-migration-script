FROM python:3.11.10-bookworm

WORKDIR /usr/src/migration

RUN pip install --no-cache-dir kubernetes flask

COPY migrate.py .

ENTRYPOINT [ "python", "./migrate.py" ]

