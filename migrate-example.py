import requests
url = 'http://192.168.194.86:5000/migrate'
#url = 'http://localhost:5001/migrate'
data_pod = {
    'node': 'k8s-worker2',
    'pod': 'nginx',
    'keep_pod_name': True,
}

data_deployment = {
    'node': 'k8s-worker1',
    'deployment': 'nginx-deployment'
}

print("now migrating pod", data_pod['pod'], "to node", data_pod['node'])
response = requests.post(url, json=data_pod)

print("now migrating deployment", data_deployment['deployment'], "to node", data_deployment['node'])
response = requests.post(url, json=data_deployment)

print(response.status_code)
print(response.json())