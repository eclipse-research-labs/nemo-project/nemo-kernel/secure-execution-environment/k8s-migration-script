from time import sleep
from kubernetes import client, config, watch
from flask import Flask, request, jsonify

app = Flask(__name__)
# Load the Kubernetes configuration
try:
    # try local ./kube/config
    config.load_kube_config()
except:
    # try in cluster or give up
    config.load_incluster_config()

# Create a Kubernetes API client
apicore_client = client.CoreV1Api()
apiapps_client = client.AppsV1Api()

def add_label_to_node(node_name, label_key, label_value):
    # Add the label to the node
    apicore_client.patch_node(node_name, {"metadata": {"labels": {label_key: label_value}}})

def patch_depl_node_selector(deployment_name, node_selector):
    # Get the deployment

    deployment = apiapps_client.read_namespaced_deployment(deployment_name, "default")
    # Update the node selector
    deployment.spec.template.spec.node_selector = {
        "nemo-migration-to-node": node_selector
    }
    # Patch the deployment
    apiapps_client.patch_namespaced_deployment(deployment_name, "default", deployment)

def patch_pod_node_selector(pod_name, node_selector):
    # Get the pod
    pod = apicore_client.read_namespaced_pod(pod_name, "default")
    old_node = pod.spec.node_name
    # Prepare the migrated mod object
    migrated_pod = pod
    migrated_pod.metadata.name = pod_name + "-migr"
    migrated_pod.metadata.resource_version = None
    migrated_pod.spec.node_name = None
    migrated_pod.spec.node_selector = {
        "nemo-migration-to-node": node_selector
    }
    # Patch the pod with the new node selector 
    # ** This is not working, most pod object fields are immutable **
    # apicore_client.patch_namespaced_pod(pod_name, "default", pod)
    
    apicore_client.create_namespaced_pod("default", migrated_pod)
    wait_for_pod_running(pod_name + "-migr")
    print("Evicting the old pod", pod_name, "from node ", old_node)
    apicore_client.delete_namespaced_pod(pod_name, "default")

def patch_keeppod_node_selector(pod_name, node_selector):
    # Get the pod
    pod = apicore_client.read_namespaced_pod(pod_name, "default")

    # Evict the pod
    print("Evicting the old pod", pod_name, "from node ", pod.spec.node_name)
    apicore_client.delete_namespaced_pod(pod_name, "default")    
    
    # Prepare the migrated pod object
    pod.metadata.resource_version = None
    pod.spec.node_name = None
    pod.spec.node_selector = {
        "nemo-migration-to-node": node_selector
    }
    wait_for_pod_deleted(pod_name)
    # Deploy the migrating pod
    apicore_client.create_namespaced_pod("default", pod)
    pod = apicore_client.read_namespaced_pod(pod_name, "default")
    print("Pod ", pod.metadata.name, 
              "scheduled in", pod.spec.node_name,
              "with state:",  pod.status.phase)

    
def wait_for_pod_running(pod_name):
    # Watch the pod until it is running
    w = watch.Watch()
    for event in w.stream(apicore_client.list_namespaced_pod, "default"):
        if event["object"].metadata.name == pod_name:
            if event["object"].status.phase == "Running":
                print("Pod ", event["object"].metadata.name, "is running")
                w.stop()
            print("Pod ", event["object"].metadata.name, 
                  "scheduled in", event["object"].spec.node_name,
                  "with state:",  event["object"].status.phase)

def wait_for_pod_deleted(pod_name):
                    # Watch the pod until it is deleted
    w = watch.Watch()
    for event in w.stream(apicore_client.list_namespaced_pod, "default"):
        if event["object"].metadata.name == pod_name:
            if event["type"] == "DELETED":
                print("Pod ", event["object"].metadata.name, "is deleted")
                w.stop()
            print("Pod ", event["object"].metadata.name, 
                 "scheduled in", event["object"].spec.node_name,
                  "with state:",  event["object"].status.phase)

def print_all_pods():
    # List all pods in the cluster
    pods = apicore_client.list_namespaced_pod("default").items
    # Print the name of each pod
    for pod in pods:
        print("Pod ", pod.metadata.name, 
              "scheduled in", pod.spec.node_name,
              "with state:",  pod.status.phase)

def parse_arguements():
    # Parse the command-line arguments
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--node", type=str, help="The destination node to migrate")
    parser.add_argument("--pod", type=str, help="The pod to migrate")
    parser.add_argument("--keep_pod_name", type=bool, default=0, help="keep the same pod name during migration (results in downtime -- default FALSE) ")
    parser.add_argument("--deployment", type=str, help="The deployment to migrate")
    return parser.parse_args()

@app.route('/migrate', methods=['POST'])
def migrate():
    data = request.get_json()
    print(data)
    add_label_to_node(data['node'], "nemo-migration-to-node", data['node'])
    if 'deployment' in data and data['deployment'] is not None:
        patch_depl_node_selector(data['deployment'], data['node'])
    if 'pod' in data and data['pod'] is not None:
        if 'keep_pod_name' in data and data['keep_pod_name'] is not None:
            print("Keeping the same pod name during migration -- this will result in downtime")
            patch_keeppod_node_selector(data['pod'], data['node'])
        else:
            patch_pod_node_selector(data['pod'], data['node'])
    return jsonify(data)

@app.route('/health', methods=['GET'])
def health():
    return jsonify({"status": "UP"})

if __name__ == '__main__':
    
    app.run(host='0.0.0.0')

    """
    args = parse_arguements()
    print("Migrating to node: " + args.node)
    #print_all_pods()
    add_label_to_node(args.node, "nemo-migration-to-node", args.node)
    if args.deployment:
        patch_depl_node_selector(args.deployment, args.node)
    if args.pod:
        if args.keep_pod_name:
            print("Keeping the same pod name during migration -- this will result in downtime")
            patch_keeppod_node_selector(args.pod, args.node)
        else:
            patch_pod_node_selector(args.pod, args.node)
    #sleep(5)
    #print_all_pods()
    """