# SEE Migration component 

This is a Python program that implements the SEE migration component logic. The migration service runs as a Daemon in the cluster (Flask server) that accepts migration requests and migrates a given k8s deployment's PODs or a given individual pod to a given target node.

## Prerequisites
You need python3 and python kubernetes client installed in order to run this script

```bash
pip install kubernetes
```

## Package in container image

```bash
docker build -t see-migration:latest .
```

Push the image in some container registry accesible from the target Kubernetes cluster.

## Run in kubernetes 

### Create the SEE Migration component service and give it the required cluster roles:

Create a migration service to expose the migration component in the cluster:
```bash
kubectl apply -f migration-svc.yaml
```

where the migration-svc.yaml is as follows:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: migration-service
spec:
  selector:
    app: migration-pod
  ports:
  - protocol: TCP
    port: 80
    targetPort: 5000
  type: NodePort
```

Give the default service account both the "admin" and "cluster-admin" cluster roles:
```bash
kubectl create clusterrolebinding default-admin --clusterrole=admin --serviceaccount=default:default
kubectl create clusterrolebinding default-cluster-admin --clusterrole=cluster-admin --serviceaccount=default:default
```

Now create the migration pod configuration and save it in a yaml file e.g migration-pod.yaml:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: migration-pod #should be the same as the selector in migration service!
  labels:
    app: migration-pod
spec:
  containers:
    - name: migration-pod
      image: olagkasn/nemo-see-migration:daemon # change this to the registry where you pushed the migration component image.
      ports:
        - containerPort: 5000 # Port to export
```

Now you can deploy the migration component either by directly communicating with the k8s cluster (Option A) or through the NEMO SEE Interface (Option B)

### Option A: Run directly from Kubernetes

With the following command you can deploy the migration component:
```bash
kubectl apply -f migration-pod.yaml
```

Once the pod is up and running get the service IP as follows:

```bash
kubectl describe service migration-service
Name:                     migration-service
Namespace:                default
Labels:                   <none>
Annotations:              <none>
Selector:                 app=migration-pod
Type:                     NodePort
IP Family Policy:         SingleStack
IP Families:              IPv4
IP:                       10.102.198.227
IPs:                      10.102.198.227
Port:                     <unset>  80/TCP
TargetPort:               5000/TCP
NodePort:                 <unset>  31773/TCP
Endpoints:                192.168.194.86:5000
Session Affinity:         None
External Traffic Policy:  Cluster
Internal Traffic Policy:  Cluster
Events:                   <none>
```

Now you can send a POST request to the service endpoint (e.g 192.168.194.86:5000) with a JSON containing the  required parameters.
For example in Python it can be done as follows:

```python
import requests
url = 'http://192.168.194.86:5000/migrate' #use the migration servive IP / NodePort IP
#url = 'http://localhost:5001/migrate'
data_pod = {
    'node': 'k8s-worker2',
    'pod': 'nginx',
    'keep_pod_name': True,
}

data_deployment = {
    'node': 'k8s-worker1',
    'deployment': 'nginx-deployment'
}

print("now migrating pod", data_pod['pod'], "to node", data_pod['node'])
response = requests.post(url, json=data_pod)

print("now migrating deployment", data_deployment['deployment'], "to node", data_deployment['node'])
response = requests.post(url, json=data_deployment)

print(response.status_code)
print(response.json())
```

### Option B: Run using the SEE interface

Build and run the SEE interface [branch: migration-svc](https://gitlab.eclipse.org/eclipse-research-labs/nemo-project/nemo-kernel/secure-execution-environment/see-interface/-/tree/migration-svc?ref_type=heads#build-and-run-the-see-interface)

Currently the see-interface modifies Kubernetes objects by reading the configuration from yaml files.

Follow the [see-interface example](https://gitlab.eclipse.org/eclipse-research-labs/nemo-project/nemo-kernel/secure-execution-environment/see-interface/-/tree/migration-svc?ref_type=heads#modify-kubernetes-objects) and create an example pod that we are going to migrate later, using the see-ctl verbs.

Make sure that you have given the default service account both the "admin" and "cluster-admin" cluster roles and created the migration service (see previous paragraph).

Now run the migration pod using the see interface:

```bash
go run cmd/see-ctl/main.go do create -f migration-pod.yaml
```

Now the migration service is up and running and can accept requests in order to migrate other pods / deployments to different nodes. We can communicate with the migration service using the SEE interface as follows, using the "migrate" verb:

```bash
go run cmd/see-ctl/main.go do migrate -f migration-req.yaml
```
where the migration-req.yaml contains the migration request configuration in YAML format.

#### Migration Request Configuration

Here is an example request for migrating a single pod:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: migration-service
  annotations:
    node: "k8s-worker1" # target node
    pod: "nginx" # the pod name that we want to migrate
    keep_pod_name: "true" # whether to keep the pod name (slower migration time) or change it to <pod-name>-migr
```

```bash
keep_pod_name: 
- "true" will lead to a new pod in target node using the same "pod-name" as the original. Almost all fields -- including the Pod Name and Node Name -- in a pod specification are immutable. This means that we have to delete the old pod before creating the new one and as a result there will be some downtime.
- "false" will lead to a new pod called "pod-name"+migr in target\_node with no downtime
```

Example request for migrating a deployment:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: migration-service
  annotations:
    node: "k8s-worker2" # target node
    deployment: "nginx-deployment" # deployment that we want to migrate
```